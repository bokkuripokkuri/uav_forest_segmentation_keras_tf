#!/usr/bin/env python
# coding: utf-8

# In[11]:


import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
import albumentations as A
import glob, sys
from tqdm import tqdm
import rasterio


# In[ ]:





# In[65]:


def convIndexColor_fromNp(array):
    paletteImg = Image.open("./colorPalette.png")
    palette = paletteImg.getpalette()
    msk = Image.fromarray(array.astype(np.int8)).convert("P")
    msk.putpalette(palette)
    return msk


# In[70]:


datasetPath = ".\\dataset_Seg\\RingyouC_Forest_Drone_SugiHinoki_2Class\\"

orgDirs = datasetPath + "orgImage\\"
orgIdxDirs = datasetPath + "orgImage_IndexColor\\"
mskDirs = datasetPath + "mskImage\\"
mskIdxDirs = datasetPath + "mskImage_IndexColor\\"

# orgImages = glob.glob(orgIdxDirs + "*")
# mskImages = glob.glob(mskIdxDirs + "*")
orgImages = glob.glob(orgDirs + "*")
mskImages = glob.glob(mskDirs + "*")


# In[71]:


cropSize = 1024
lapSize = 0


# In[72]:


print(orgImages[0],mskImages[0])


# In[73]:


def getXYtileInfo(predictImgPath,cropSize,lapSize):
    
    img_io = rasterio.open(predictImgPath)
    org_height, org_width, ch = img_io.height, img_io.width, img_io.count
    img_io.close()

    predictImgSize = cropSize


    if lapSize == 0:
        step_x_col = org_width//predictImgSize
        step_y_row = org_height//predictImgSize
    else:
        step_x_col = (org_width - (predictImgSize - lapSize))// lapSize
        step_y_row = (org_height - (predictImgSize - lapSize))// lapSize
    # step_x_col = org_width - (org_width % lapSize))
    # step_y_row = org_height - (org_height % lapSize)


    if lapSize==0:
        resizeSetSlide = ((org_height // cropSize) * cropSize, org_width // cropSize * cropSize)
    else:
        resizeSetSlide = (org_height // lapSize * lapSize, org_width // lapSize * lapSize)

    return step_x_col, step_y_row, resizeSetSlide


def cropImg_lapsize(imgPath,cropSize,lapSize):

    # imgNp_true = imgNp_true.reshape([org_height, org_width, ch]).copy()

    
    # print("imgNp_true.shape", imgNp_true.shape)

    img = Image.open(imgPath)

    step_x_col, step_y_row, resizeSetSlide = getXYtileInfo(imgPath, cropSize, lapSize)
    print("col_step, row_step, resizeSetSlide ",step_x_col, step_y_row, resizeSetSlide)

    img_resize = img.resize(resizeSetSlide)

    imgNp_resized = np.array(img_resize)
    # imgNp_true_resized[height : width : ch]の構造　 imgNp_true_resized.shape = (h, w, ch)

    print("Resize Image SIze", imgNp_resized.shape)

    del img


    print("Create Tiles ImageSize:{0}, LapSize:{1}".format(cropSize, lapSize))
    for y in tqdm(range(step_y_row)):
        # print("\n\n")
        for x in tqdm(range(step_x_col)):

            # print("\n","x, y =", x, y )
            if lapSize==0:
                x_start , x_end = x*cropSize, x*cropSize + cropSize
                y_start , y_end = y*cropSize, y*cropSize + cropSize
            else:
                x_start , x_end = x*lapSize , x*lapSize + cropSize
                y_start , y_end = y*lapSize , y*lapSize + cropSize

            # imgNp_resized[height : width : ch]の構造　 imgNp_true.shape = (h, w, ch)
            # (predictImgSize, predictImgSize, 3) で取り出す　他サイズでは出ない

            xyStr = "X" + str(x_start).zfill(5) + "to" + str(x_end).zfill(5) + "_" + "Y" + str(y_start).zfill(5) + "to" + str(y_end).zfill(5)
            # print("Makedict imgNp", imgNp.shape, xyStr)
            saveImgDIr = os.path.dirname(imgPath) + "_Size" +str(cropSize).zfill(4) + "_lap" + str(lapSize).zfill(4) + "\\"
            os.makedirs(saveImgDIr, exist_ok=True)
            saveImgPath = saveImgDIr + os.path.basename(imgPath).split(".")[-2] + "_" + xyStr + "." + os.path.basename(imgPath).split(".")[-1] 
            
            if img_resize.mode == 'RGB':
                imgNp = imgNp_resized[x_start : x_end, y_start : y_end, :].copy()
                crpoImg_org = Image.fromarray(imgNp)
                crpoImg_org.save(saveImgPath)
                
            else:
                imgNp = imgNp_resized[x_start : x_end, y_start : y_end].copy()
                if "Index" in imgPath:
                    cropImg_msk = convIndexColor_fromNp(imgNp)
                else:
                    cropImg_msk = Image.fromarray(imgNp)
                cropImg_msk.save(saveImgPath)


# In[ ]:


for orgIdxPath in tqdm(orgImages):
    cropImg_lapsize(orgIdxPath,cropSize,lapSize)
    mskIdxPath = orgIdxPath.replace("org","msk").replace(".jpg",".png")
    cropImg_lapsize(mskIdxPath,cropSize,lapSize)
