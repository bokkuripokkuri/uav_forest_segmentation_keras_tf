import os

import cv2
import keras
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

from pprint import pprint

import tensorflow as tf
from keras import backend as K

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 1.0
sess = tf.Session(config=config)
K.set_session(sess)

import segmentation_models as sm

import albumentations as A

BACKBONE = 'efficientnetb3'

preprocess_input = sm.get_preprocessing(BACKBONE)

import rasterio, gdal

import glob, sys, os
from tqdm import tqdm

import Func_Unet_Satelite as mf
import preprocess as mypre
import visualization as myvisi

import preprocess



def load_unetModel(modelFilePath, CLASSES, BACKBONE):
    # predictImgSize = 1024
    # lapSize = 0

    # define network parameters
    n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'

    #create model
    model = sm.Unet(BACKBONE, classes=n_classes, activation=activation)

    # print("\n","model.summary:",modelFilePath,"\n")
    model.load_weights(modelFilePath) 
    # model.load_weights(resultDir + 'best_unet_model_imgSize' + str(imgSize) + '.h5') 

    print("Model Weight Loaded",modelFilePath)
    return model



def load_pspnetModel(modelFilePath, CLASSES):
    # load best weights

    #preiadesFalseColor
    # modelPath = glob.glob("./result/20200522_1234_Batch8_imgSize256_Epoch100/*.h5")[0]

    # predictImgSize = 1024
    # lapSize = 0

    # define network parameters
    n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'

    #create model
    model = sm.PSPNet(BACKBONE, classes=n_classes, activation=activation)
    # model = sm.PSPNet(BACKBONE, classes=n_classes, activation=activation)
    # pprint.pprint(get_gpu_info())
    
    print("\n","model.summary:",model.summary(),"\n")

    model.load_weights(modelFilePath) 
    # model.load_weights(resultDir + 'best_unet_model_imgSize' + str(imgSize) + '.h5') 

    print("Model Weight Loaded",modelFilePath)
    return model





def load_unetModel_tf(modelFilePath, CLASSES):
    # load best weights

    #preiadesFalseColor
    # modelPath = glob.glob("./result/20200522_1234_Batch8_imgSize256_Epoch100/*.h5")[0]

    # predictImgSize = 1024
    # lapSize = 0


    from util import model
    from util import loader
    
    # Create a model
    model_unet = model.UNet(l2_reg=0.0001).model


    saver = tf.train.Saver()


    with tf.Session() as sess:

        saver.restore(sess, modelFilePath + "modelunet_model.ckpt")

        print("Model Loaded ",type(sess))


        return sess


    # define network parameters
    n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
    activation = 'sigmoid' if n_classes == 1 else 'softmax'

    #create model
    model = sm.Unet(BACKBONE, classes=n_classes, activation=activation)
    # model = sm.PSPNet(BACKBONE, classes=n_classes, activation=activation)
    # pprint.pprint(get_gpu_info())


    # print("\n","model.summary:",modelFilePath,"\n")
    model.load_weights(modelFilePath) 
    # model.load_weights(resultDir + 'best_unet_model_imgSize' + str(imgSize) + '.h5') 

    print("Model Weight Loaded",modelFilePath)
    return model
