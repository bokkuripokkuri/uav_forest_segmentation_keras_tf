import numpy as np
# %matplotlib inline?#???????????????????
import matplotlib.pyplot as plt
import os
from PIL import Image
import cv2



# モルフォロジー変換
# http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html

def imgNoizeDel(img_np_gray, kernel_pixelsize:int):
    kernel = np.ones((kernel_pixelsize, kernel_pixelsize), np.uint8)
    #Opening
    img_np_gray = cv2.morphologyEx(img_np_gray, cv2.MORPH_OPEN, kernel)
    # Closing
    img_np_gray = cv2.morphologyEx(img_np_gray, cv2.MORPH_CLOSE, kernel)
    return img_np_gray


# 画像の平滑化
# http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/py_tutorials/py_imgproc/py_filtering/py_filtering.html

def imgFilter(img_np_gray, kernel_pixelsize:int):
    kernel = np.ones((kernel_pixelsize,kernel_pixelsize),np.float32)/(kernel_pixelsize*kernel_pixelsize)
    img_np_gray_filtered = cv2.filter2D(img_np_gray,-1,kernel)
    return img_np_gray_filtered

def imgBlur(img_np_gray, kernel_pixelsize:int):
    return cv2.blur(img_np_gray,-1,(kernel_pixelsize,kernel_pixelsize))


# blur = cv2.GaussianBlur(img,(5,5),0)

# median = cv2.medianBlur(img,5)

# blur = cv2.bilateralFilter(img,9,75,75)